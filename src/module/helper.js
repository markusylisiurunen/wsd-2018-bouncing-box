/**
 * @overview General helper functions.
 */

/**
 * Create a new renderer for a canvas.
 * @param  {Object} canvas Canvas to render to.
 * @return {Object}        Scoped renderer for the canvas.
 */
export function createRenderer(canvas) {
  const context = canvas.getContext('2d');

  return {
    /**
     * Draw a rectangle to the canvas.
     * @param {Object}  _           Parameters.
     * @param {Number}  _.x         x-coordinate for the rectangle.
     * @param {Number}  _.y         y-coordinate for the rectangle.
     * @param {Number}  _.width     Width of the rectangle.
     * @param {Number}  _.height    Height of the rectangle.
     * @param {String}  _.color     Color to draw with.
     * @param {Boolean} _.fill      Whether to fill the rectangle or not.
     * @param {Number}  _.lineWidth Line width to draw with.
     * @param {Boolean} _.centered  Control (x, y) coordinate meaning.
     * @param {Boolean} _.relative  Whether coordinates are relative to canvas.
     */
    drawRect({
      x,
      y,
      width,
      height,
      color,
      fill = false,
      lineWidth = 6,
      centered = false,
      relative = false,
    }) {
      context.lineWidth = lineWidth;
      context.fillStyle = color;
      context.strokeStyle = color;

      let posX = x;
      let posY = y;
      let sizeWidth = width;
      let sizeHeight = height;

      if (relative) {
        posX = canvas.width * x;
        posY = canvas.height * y;
        sizeWidth = canvas.width * width;
        sizeHeight = canvas.height * height;
      }

      if (centered) {
        posX -= 0.5 * sizeWidth;
        posY -= 0.5 * sizeHeight;
      }

      if (fill) {
        context.fillRect(posX, posY, sizeWidth, sizeHeight);
      } else {
        context.strokeRect(posX, posY, sizeWidth, sizeHeight);
      }
    },

    /**
     * Draw text to the canvas.
     * @param {Object}  _           Parameters.
     * @param {Number}  _.x         x-coordinate for the text.
     * @param {Number}  _.y         y-coordinate for the text.
     * @param {String}  _.text      Text to draw.
     * @param {String}  _.font      Font to use for the text.
     * @param {String}  _.color     Color to draw with.
     * @param {Number}  _.maxWidth  Max width of the text block.
     * @param {String}  _.align     Alignment of the text.
     * @param {Boolean} _.relative  Whether coordinates are relative to canvas.
     */
    drawText({
      x,
      y,
      text,
      font,
      color,
      maxWidth = null,
      align = 'start',
      relative = false,
    }) {
      context.font = font;
      context.textAlign = align;
      context.fillStyle = color;

      let posX = x;
      let posY = y;

      if (relative) {
        posX = canvas.width * x;
        posY = canvas.height * y;
      }

      if (maxWidth) {
        context.fillText(text, posX, posY, maxWidth);
      } else {
        context.fillText(text, posX, posY);
      }
    },
  };
}

/**
 * Clamp a number to a range.
 * @param  {Number} min Lower boundary.
 * @param  {Number} max Upper boundary.
 * @param  {Number} num Number to clamp.
 * @return {Number}     Clamped number.
 */
export function clampNumber(min, max, num) {
  // eslint-disable-next-line no-nested-ternary
  return num <= min ? min : num >= max ? max : num;
}

/**
 * Generate a random color.
 * @return {String} Generated color.
 */
export function generateRandomColor() {
  const options = '6789abcd'.split('');
  let color = '#';

  for (let i = 0; i < 6; i += 1) {
    color += options[Math.floor(options.length * Math.random())];
  }

  return color;
}

/**
 * Check whether the site is running inside an iframe.
 * @return {Boolean} True if in an iframe.
 */
export function isInIframe() {
  return window.parent !== window;
}
