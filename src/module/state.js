/**
 * @overview Manage the state of the game.
 */

import message from './message';
import { isInIframe } from './helper';

let state = {};

export default {
  /**
   * Initialise the state.
   * @param {Object} initialState The initial state.
   */
  init(initialState) {
    state = initialState;
  },

  /**
   * Update the state with a new value.
   * @param {String} name  Name of the state.
   * @param {Mixed}  value The value to be set.
   */
  set(name, value) {
    state[name] = value;

    if (!isInIframe()) {
      window.localStorage.setItem('gameState', JSON.stringify(state));
    }

    message.send({ type: 'SAVE', data: { gameState: state } });
  },

  /**
   * Get a state's value.
   * @param  {String} name Name of the attribute to get.
   * @return {Mixed}       Value for the attribute, or null.
   */
  get(name) {
    return state[name] || null;
  },
};
