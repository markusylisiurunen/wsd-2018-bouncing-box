/**
 * @overview Manage the communication between the game and the store.
 */

import { isInIframe } from './helper';

export default {
  /**
   * Send a new event to the game store.
   * @param {Object} _      Parameters.
   * @param {String} _.type Type of the sent event.
   * @param {Object} _.data Data to send along the event.
   *
   */
  send({ type, data = {} }) {
    const message = { messageType: type, ...data };

    // eslint-disable-next-line no-console
    if (!isInIframe()) console.log(JSON.stringify(message));
    else window.parent.postMessage(message, '*');
  },

  /**
   * Attach a new listener for the events coming from the store.
   * @param  {Function} handler Handler function for the event.
   * @return {Function}         Function to unattach the handler.
   */
  on(handler) {
    if (!isInIframe()) return () => {};

    const listener = evt => handler(evt.data);
    window.addEventListener('message', listener, false);

    return window.removeEventListener.bind(window, 'message', listener);
  },
};
