/**
 * @overview Manage the theme of the game.
 */

import events from './events';
import state from './state';
import { generateRandomColor } from './helper';

// Define "module" for each available theme.
const themes = {
  dark: {
    /**
     * Activate the dark theme.
     */
    activate() {
      state.set('themeName', 'dark');
    },

    /**
     * Get colors for the dark theme.
     * @return {Object} Colors for the theme.
     */
    getColors() {
      return { foreground: '#ffffff', background: '#000000' };
    },
  },
  light: {
    /**
     * Activate the light theme.
     */
    activate() {
      state.set('themeName', 'light');
    },

    /**
     * Get colors for the light theme.
     * @return {Object} Colors for the theme.
     */
    getColors() {
      return { foreground: '#000000', background: '#ffffff' };
    },
  },
  strobo: {
    // Options for the theme
    options: {
      interval: null,
      colors: {
        foreground: null,
        background: null,
      },
    },

    /**
     * Initialise the strobo theme.
     */
    init() {
      const updateColors = () =>
        ['foreground', 'background'].forEach(c => {
          this.options.colors[c] = generateRandomColor();
        });

      updateColors();
      this.options.interval = setInterval(updateColors, 75);
    },

    /**
     * Activate the strobo theme.
     */
    activate() {
      state.set('themeName', 'strobo');
      this.init();
    },

    /**
     * Deactivate the strobo theme.
     */
    deactivate() {
      clearInterval(this.options.interval);
    },

    /**
     * Get colors for the strobo theme.
     * @return {Object} Colors for the theme.
     */
    getColors() {
      return this.options.colors;
    },
  },
};

/**
 * Call a theme lifecycle method safely.
 * @param {String} theme  Name of the target theme.
 * @param {String} method Name of the lifecycle method.
 */
const safeCallThemeLifecycle = (theme, method) => {
  if (typeof themes[theme][method] === 'function') {
    themes[theme][method]();
  }
};

// Initialise the active theme on init
events.on(type => {
  if (type !== 'init') return;

  const activeTheme = state.get('themeName');
  const currentTheme = activeTheme || Object.keys(themes)[0];

  if (activeTheme === null) state.set('themeName', currentTheme);

  safeCallThemeLifecycle(currentTheme, 'init');
});

export default {
  /**
   * Change the theme to next next available theme.
   */
  next() {
    let theme = state.get('themeName');
    const themeNames = Object.keys(themes);
    const themeIndex = themeNames.indexOf(theme);

    safeCallThemeLifecycle(theme, 'deactivate');

    if (themeIndex === themeNames.length - 1) {
      [theme] = themeNames;
    } else {
      theme = themeNames[themeIndex + 1];
    }

    safeCallThemeLifecycle(theme, 'activate');
  },

  /**
   * Get the active colors for the theme.
   * @return {Object} Active theme's colors.
   */
  getColors() {
    return themes[state.get('themeName')].getColors();
  },
};
