/**
 * @overview Manage collision boxes and detect collisions between groups.
 */

let objects = {};

/**
 * Check collision boxes of two objects and check if they collide.
 * @param  {Object}  a First collision box.
 * @param  {Object}  b Second collision box.
 * @return {Boolean}   True if collided.
 */
const checkCollision = (a, b) =>
  a.x < b.x + b.width &&
  a.x + a.width > b.x &&
  a.y < b.y + b.height &&
  a.height + a.y > b.y;

export default {
  /**
   * Clear all objects from memory.
   */
  clear() {
    objects = {};
  },

  /**
   * Add new collision box.
   * @param {Object} _        Parameters.
   * @param {String} _.type   Group name.
   * @param {Number} _.x      Coordinate of the left upper corner.
   * @param {Number} _.y      Coordinate of the left upper corner.
   * @param {Number} _.width  Width of the collision box.
   * @param {Number} _.height Height of the collision box.
   */
  add({ type, x, y, width, height }) {
    if (!objects[type]) {
      objects[type] = [];
    }

    objects[type].push({ x, y, width, height });
  },

  /**
   * Check whether two groups collide.
   * @param  {Object}  _      Parameters.
   * @param  {String}  _.from First group.
   * @param  {String}  _.to   Second group.
   * @return {Boolean}        True if collision detected.
   */
  collides({ from, to }) {
    const fromObjects = objects[from] || [];
    const toObjects = objects[to] || [];

    for (let i = 0; i < fromObjects.length; i += 1) {
      for (let u = 0; u < toObjects.length; u += 1) {
        if (checkCollision(fromObjects[i], toObjects[u])) {
          return true;
        }
      }
    }

    return false;
  },
};
