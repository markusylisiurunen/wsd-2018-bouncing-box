/**
 * @overview Global event bus.
 */

const listeners = [];

export default {
  /**
   * Attach a new listener to the event bus.
   * @param {Function} listener Listener function.
   */
  on(listener) {
    listeners.push(listener);
  },

  /**
   * Send a new event through the event bus.
   * @param {String} type Type of the event.
   * @param {Mixed}  data Data to be sent along the event.
   */
  send(type, data = null) {
    listeners.forEach(listener => listener(type, data));
  },
};
