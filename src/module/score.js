/**
 * @overview Manage last and high score.
 */

import events from './events';
import state from './state';

const scores = { last: 0, high: 0 };

// Initialise the score from the stored data
events.on(type => {
  if (type !== 'init') return;
  scores.high = state.get('highScore') || 0;
});

export default {
  // Constants for the types
  LAST: 'last',
  HIGH: 'high',

  /**
   * Get the last or the high score of the player.
   * @param  {String} type Type of the score.
   * @return {Number}      Stored value for the score.
   */
  get(type) {
    return scores[type];
  },

  /**
   * Set a value for a score.
   * @param {String} type  Type of the score to set.
   * @param {Number} score New value for the score.
   */
  set(type, score) {
    scores[type] = score;
    if (type === this.HIGH) state.set('highScore', score);
  },
};
