/**
 * @overview Manage input from the player.
 */

import events from './events';

let canvas = null;

// Initialise the canvas on init
events.on(type => {
  if (type !== 'init') return;
  canvas = document.getElementById('game');
});

// Define native events
const native = ['click'];

export default {
  /**
   * Attach an event handler on an input event.
   * @param  {String}   name    Name of the event to listen to.
   * @param  {Function} handler Handler function.
   * @return {Function}         Function to clear the subscription.
   */
  on(name, handler) {
    const isNative = native.includes(name);
    const eventName = (isNative && name) || 'keydown';
    const listener = e => (isNative || e.key === name) && handler(e);

    canvas.addEventListener(eventName, listener);

    return canvas.removeEventListener.bind(canvas, eventName, listener);
  },
};
