/**
 * @overview Entry point for the game.
 *
 * This starts the render loop and manages active scenes. It also initialises
 * the data using the `message` module.
 */

import events from './module/events';
import message from './module/message';
import state from './module/state';
import { isInIframe } from './module/helper';
import scenes from './scene';

/**
 * Resize the canvas to match the window size.
 * @param {Object} canvas        Canvas to resize.
 * @param {Number} scalingFactor Scaling factor for high resolution.
 */
const updateCanvasSize = (canvas, scalingFactor) => {
  /* eslint-disable no-param-reassign */
  canvas.width = window.innerWidth * scalingFactor;
  canvas.height = window.innerHeight * scalingFactor;
  /* eslint-enable no-param-reassign */
};

/**
 * Start rendering the game.
 * @param {Object} canvas Canvas to render to.
 */
const startRenderCycle = canvas => {
  let activeSceneName = 'menu';
  let activeScene = null;

  let lastRenderTime = null;

  const renderNextFrame = () => {
    if (!activeScene) {
      activeScene = new scenes[activeSceneName]({
        canvas,
        changeScene: name => {
          activeScene.destroy();

          activeSceneName = name;
          activeScene = null;
        },
      });
    }

    const now = Date.now();

    activeScene.render({ sinceLastRender: now - lastRenderTime });
    lastRenderTime = now;

    window.requestAnimationFrame(renderNextFrame);
  };

  window.requestAnimationFrame(renderNextFrame);
};

/**
 * Initialise the game with initialisation data.
 * @param {Object} canvas       Canvas to render to.
 * @param {Object} initialState Stored game state.
 */
const init = (canvas, initialState) => {
  message.send({
    type: 'SETTING',
    data: { options: { width: 600, height: 600 } },
  });

  state.init(initialState);
  events.send('init');

  startRenderCycle(canvas);
};

window.addEventListener('load', () => {
  const canvas = document.getElementById('game');

  canvas.focus();

  updateCanvasSize(canvas, 2);

  window.addEventListener('resize', () => updateCanvasSize(canvas, 2), {
    passive: false,
  });

  message.on(evt => {
    const { messageType, gameState = {}, info } = evt;

    if (!['LOAD', 'ERROR'].includes(messageType)) return;

    // TODO: What should happen on error?
    if (messageType === 'ERROR') {
      // eslint-disable-next-line no-console
      console.log(`ERROR: ${info}`);
    }

    init(canvas, gameState);
  });

  message.send({ type: 'LOAD_REQUEST' });

  if (!isInIframe()) {
    const initialState =
      JSON.parse(window.localStorage.getItem('gameState')) || {};
    init(canvas, initialState);
  }
});
