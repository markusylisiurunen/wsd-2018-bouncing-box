/**
 * @overview Player object.
 */

import theme from '../module/theme';
import input from '../module/input';
import collision from '../module/collision';
import { createRenderer } from '../module/helper';

class Player {
  /**
   * @class
   * @param {Object} _        Parameters.
   * @param {Object} _.canvas Canvas to render to.
   */
  constructor({ canvas }) {
    this.canvas = canvas;
    this.renderer = createRenderer(canvas);

    this.positionY = canvas.height * 0.2;
    this.velocity = 0;

    this.config = {
      positionX: 100,
      size: 80,
      jump: 800,
      gravity: 2000,
    };

    this.inputSubscriptions = [
      ...['click', ' ', 'ArrowUp', 'w'].map(key =>
        input.on(key, this.jump.bind(this))
      ),
    ];
  }

  /**
   * Destroy all resources related to this object.
   */
  destroy() {
    this.inputSubscriptions.forEach(clearSubscription => clearSubscription());
  }

  /**
   * Make the player jump.
   */
  jump() {
    this.velocity = -1 * this.config.jump;
  }

  /**
   * Update the position of the player and player's collision box.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  update({ sinceLastRender }) {
    this.velocity += sinceLastRender / 1000 * this.config.gravity;
    this.positionY += sinceLastRender / 1000 * this.velocity;

    collision.add({
      type: 'player',
      x: this.config.positionX / this.canvas.width,
      y: this.positionY / this.canvas.height,
      width: this.config.size / this.canvas.width,
      height: this.config.size / this.canvas.height,
    });
  }

  /**
   * Render the player to the canvas.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  render({ sinceLastRender }) {
    this.update({ sinceLastRender });

    this.renderer.drawRect({
      x: this.config.positionX,
      y: this.positionY,
      width: this.config.size,
      height: this.config.size,
      color: theme.getColors().foreground,
    });
  }
}

export default Player;
