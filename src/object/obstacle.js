/**
 * @overview Collidable obstacle object.
 */

import theme from '../module/theme';
import score from '../module/score';
import collision from '../module/collision';
import { createRenderer } from '../module/helper';

/**
 * Generate a random number in a range.
 * @param  {Number} low  Lower boundary.
 * @param  {Number} high Upper boundary.
 * @return {Number}      Generated number.
 */
const randomNumberBetween = (low, high) => low + Math.random() * (high - low);

class Obstacle {
  /**
   * @class
   * @param {Object} _          Parameters.
   * @param {Object} _.canvas   Canvas to render to.
   * @param {Number} _.position Initial position of the obstacle.
   */
  constructor({ canvas, position = null }) {
    this.canvas = canvas;
    this.renderer = createRenderer(canvas);

    this.positionX = position !== null ? position : canvas.width;

    this.config = {
      width: 125,
      velocity: 350,
      padding: 75,
      gap: {
        size: 270,
        position: null,
      },
    };

    this.config.gap.position = randomNumberBetween(
      this.config.padding / this.canvas.height + 0.1,
      1 -
        this.config.padding / this.canvas.height -
        this.config.gap.size / this.canvas.height -
        0.1
    );
  }

  /**
   * Get current position of the obstacle.
   * @return {Number} Current x-coordinate of the position.
   */
  getPosition() {
    return this.positionX;
  }

  /**
   * Get the top block bounding box of the obstacle.
   */
  getFirstBlock() {
    return {
      x: this.positionX / this.canvas.width,
      y: this.config.padding / this.canvas.height,
      width: this.config.width / this.canvas.width,
      height:
        this.config.gap.position - this.config.padding / this.canvas.height,
    };
  }

  /**
   * Get the bottom block bounding box of the obstacle.
   */
  getSecondBlock() {
    return {
      x: this.positionX / this.canvas.width,
      y: this.config.gap.position + this.config.gap.size / this.canvas.height,
      width: this.config.width / this.canvas.width,
      height:
        1 -
        this.config.padding / this.canvas.height -
        this.config.gap.position -
        this.config.gap.size / this.canvas.height,
    };
  }

  /**
   * Update the position of the block and collision boxes.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  update({ sinceLastRender }) {
    const lastPositionX = this.positionX;
    this.positionX -= sinceLastRender / 1000 * this.config.velocity;

    if (lastPositionX >= 0 && this.positionX < 0) {
      score.set('last', score.get('last') + 1);
    }

    collision.add({ type: 'obstacle', ...this.getFirstBlock() });
    collision.add({ type: 'obstacle', ...this.getSecondBlock() });
  }

  /**
   * Render the block to the canvas.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  render({ sinceLastRender }) {
    this.update({ sinceLastRender });

    const { foreground } = theme.getColors();

    this.renderer.drawRect({
      ...this.getFirstBlock(),
      color: foreground,
      relative: true,
    });

    this.renderer.drawRect({
      ...this.getSecondBlock(),
      color: foreground,
      relative: true,
    });
  }
}

export default Obstacle;
