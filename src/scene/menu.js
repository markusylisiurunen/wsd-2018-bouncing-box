/**
 * @overview Scene for the menu.
 */

import theme from '../module/theme';
import score from '../module/score';
import input from '../module/input';
import { createRenderer, clampNumber } from '../module/helper';

export default class Menu {
  /**
   * @class
   * @param {Object}   _             Parameters.
   * @param {Object}   _.canvas      Canvas to render to.
   * @param {Function} _.changeScene Method for changing to another scene.
   */
  constructor({ canvas, changeScene }) {
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
    this.changeScene = changeScene;

    this.renderer = createRenderer(canvas);

    this.objects = [];

    this.inputSubscriptions = [
      input.on('t', theme.next),
      input.on('click', this.onClick.bind(this)),
      input.on(' ', this.startGame.bind(this)),
    ];
  }

  /**
   * Destroy all resources related to this scene.
   */
  destroy() {
    this.inputSubscriptions.forEach(clearSubscription => clearSubscription());
    this.objects.forEach(obj => obj.destroy());
  }

  /**
   * Handles clicks on the canvas.
   * @param {Object} evt Click event.
   */
  onClick(evt) {
    const { clientX, clientY } = evt;
    const x1 = 0;
    const x2 = window.innerWidth;
    const y1 = window.innerHeight * 0.9;
    const y2 = window.innerHeight;

    if (clientX > x1 && clientX < x2 && clientY > y1 && clientY < y2) {
      theme.next();
      return;
    }

    this.startGame();
  }

  /**
   * Start the game.
   */
  startGame() {
    this.changeScene('game');
  }

  /**
   * Render this scene to an empty canvas.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  render({ sinceLastRender }) {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    const { foreground, background } = theme.getColors();

    this.renderer.drawRect({
      x: 0,
      y: 0,
      width: 1,
      height: 1,
      color: background,
      fill: true,
      relative: true,
    });

    this.renderer.drawText({
      text: 'Bouncing Box',
      font: `bold ${clampNumber(
        90,
        180,
        this.canvas.width * 0.1
      )}px sans-serif`,
      align: 'center',
      color: foreground,
      x: 0.5,
      y: 0.4,
      relative: true,
    });

    this.renderer.drawText({
      text: 'Click to start the game',
      font: `${clampNumber(36, 48, this.canvas.width * 0.03)}px sans-serif`,
      align: 'center',
      color: foreground,
      x: 0.5,
      y: 0.55,
      relative: true,
    });

    this.renderer.drawText({
      text: `High score: ${score.get('high')}`,
      font: `${clampNumber(28, 36, this.canvas.width * 0.03)}px sans-serif`,
      align: 'start',
      color: foreground,
      x: 0.05,
      y: 0.05,
      relative: true,
    });

    this.renderer.drawText({
      text: `Score: ${score.get('last')}`,
      font: `${clampNumber(28, 36, this.canvas.width * 0.03)}px sans-serif`,
      align: 'end',
      color: foreground,
      x: 0.95,
      y: 0.05,
      relative: true,
    });

    this.renderer.drawText({
      text: 'Click here or press "t" to change theme',
      font: `${clampNumber(28, 36, this.canvas.width * 0.03)}px sans-serif`,
      align: 'center',
      color: foreground,
      x: 0.5,
      y: 0.95,
      relative: true,
    });

    this.objects.forEach(obj => {
      obj.render({ sinceLastRender });
    });
  }
}
