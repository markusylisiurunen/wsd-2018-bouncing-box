/**
 * @overview Scene for the actual gameplay.
 */

import theme from '../module/theme';
import score from '../module/score';
import collision from '../module/collision';
import message from '../module/message';
import { createRenderer, clampNumber } from '../module/helper';
import Player from '../object/player';
import Obstacle from '../object/obstacle';

export default class Game {
  /**
   * @class
   * @param {Object}   _             Parameters.
   * @param {Object}   _.canvas      Canvas to render to.
   * @param {Function} _.changeScene Method for changing to another scene.
   */
  constructor({ canvas, changeScene }) {
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
    this.changeScene = changeScene;

    this.renderer = createRenderer(canvas);

    this.hasCollided = false;
    this.obstacleThreshold = 800;

    this.objects = [new Player({ canvas })];

    do {
      const last =
        this.objects.length === 1
          ? null
          : this.objects[this.objects.length - 1];

      this.createObstacle(
        this.obstacleThreshold + (last ? last.getPosition() : 150)
      );
    } while (
      this.objects[this.objects.length - 1].getPosition() < canvas.width
    );

    score.set('last', 0);
  }

  /**
   * Destroy all resources related to this scene.
   */
  destroy() {
    this.objects.forEach(
      obj => typeof obj.destroy === 'function' && obj.destroy()
    );

    collision.clear();
  }

  /**
   * Create a new obstacle and add it to the level.
   * @param {Number} position Initial position of the obstacle
   */
  createObstacle(position = null) {
    this.objects.push(
      new Obstacle({ canvas: this.canvas, ...(position && { position }) })
    );
  }

  /**
   * Ensure that there are enough obstacles.
   */
  ensureObstacles() {
    let lastPosition = this.objects[this.objects.length - 1].getPosition();

    while (lastPosition < this.canvas.width - this.obstacleThreshold) {
      this.createObstacle(lastPosition + this.obstacleThreshold);
      lastPosition += this.obstacleThreshold;
    }
  }

  /**
   * Collision detected, die.
   */
  die() {
    if (score.get('last') > score.get('high')) {
      score.set('high', score.get('last'));
    }

    this.hasCollided = true;

    message.send({
      type: 'SCORE',
      data: { score: parseFloat(score.get('last')) },
    });

    setTimeout(() => this.changeScene('menu'), 500);
  }

  /**
   * Render this scene to an empty canvas.
   * @param {Object} _                 Parameters.
   * @param {Number} _.sinceLastRender Milliseconds since last render.
   */
  render({ sinceLastRender }) {
    if (this.hasCollided) {
      return;
    }

    if (
      collision.collides({ from: 'player', to: 'obstacle' }) ||
      collision.collides({ from: 'player', to: 'level' })
    ) {
      this.die();
    }

    collision.clear();

    collision.add({ type: 'level', x: 0, y: -10, width: 1, height: 10 });
    collision.add({ type: 'level', x: 0, y: 1, width: 1, height: 10 });

    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    const { foreground, background } = theme.getColors();

    this.renderer.drawRect({
      x: 0,
      y: 0,
      width: 1,
      height: 1,
      color: background,
      fill: true,
      relative: true,
    });

    this.renderer.drawText({
      text: `High score: ${score.get('high')}`,
      font: `${clampNumber(28, 36, this.canvas.width * 0.03)}px sans-serif`,
      align: 'start',
      color: foreground,
      x: 0.05,
      y: 0.05,
      relative: true,
    });

    this.renderer.drawText({
      text: `Score: ${score.get('last')}`,
      font: `${clampNumber(28, 36, this.canvas.width * 0.03)}px sans-serif`,
      align: 'end',
      color: foreground,
      x: 0.95,
      y: 0.05,
      relative: true,
    });

    this.ensureObstacles();

    this.objects.forEach(obj => {
      obj.render({ sinceLastRender });
    });
  }
}
