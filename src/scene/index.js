/**
 * @overview Gathers all scenes into one module.
 */

import menu from './menu';
import game from './game';

export default { menu, game };
