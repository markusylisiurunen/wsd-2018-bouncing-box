# 📦 Bouncing Box

A flappy bird clone for a web development course (TIE-23500) built with Vanilla JS. The deployed
version of this game can be found [here](https://wsd-2018-bouncing-box.now.sh).

Please note that the game is optimised for smaller screen sizes due to the nature of the course
assignment. Therefore it works best on mobile phones :)
